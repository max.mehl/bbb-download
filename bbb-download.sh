#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-only

URL="$1"
TMP=$(mktemp -d)

# Check dependencies
check () { command -v $1 $> /dev/null; }
for cmd in perl wget ffmpeg; do
  if ! check $cmd; then
    echo "ERROR! $cmd is not installed on your system"
    exit 1
  fi
done

# Extract URL and ID
ID=$(echo $URL | perl -pe 's/.*(?<=meetingId=)([0-9a-z-]*$)/\1/')
URL=$(echo $URL | grep -Eo '(http|https)://[^/"]+')

# Download video parts
echo "Downloading video 1..."
wget -qO "$TMP/deskshare.webm" "$URL/presentation/$ID/deskshare/deskshare.webm"

echo "Downloading video 2..."
wget -qO "$TMP/webcams.webm" "$URL/presentation/$ID/video/webcams.webm"

# Convert video
echo "Converting video: merge audio..."
ffmpeg -hide_banner -loglevel error -i "$TMP/webcams.webm" -i "$TMP/deskshare.webm" -c copy "$ID.webm" 

echo "Finished! File has been written to $(pwd)/$ID.webm"

rm -r "$TMP"
