<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-only
-->

# BBB Recording Downloader

A simple script to download a public recording of a BigBlueButton instance.

The resulting file only contains the screenshare and the audio, no webcams.

## Setup

This should work on all GNU/Linux systems. You have to have `perl`, `wget` and `ffmpeg` installed.


## Usage

Get the URL of the recording. If you can watch the recording in your browser, this URL should be it.

The URL may look like the following: `https://conf.fsfe.org/playback/presentation/2.0/playback.html?meetingId=123abc456def-789ghi0j`

Then, in your terminal (replace the URL obviously!):

```sh
./bbb-download.sh https://conf.fsfe.org/playback/presentation/2.0/playback.html?meetingId=123abc456def-789ghi0j
```
